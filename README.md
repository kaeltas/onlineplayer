# README #
**Description:**

Online web system, which allows you to create distinct playlists of tracks from vk music database and play them back.
Playlists could be private or public. VK.API is used to search audio tracks and lately play them back.

**Used technologies: **

* Java EE 
* JSP/JSTL
* Spring (MVC, Security, Data)
* JPA, Hibernate, SQL
* HTML, JavaScript, JQuery

**Demo:**
http://jbop.herokuapp.com (first call to link may be slow, because of launching app at heroku)

user: test

pass: 12345
