package ua.pp.kaeltas.annotations;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import ua.pp.kaeltas.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by kaeltas on 06.02.15.
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class UniqueLoginValidator implements ConstraintValidator<UniqueLogin, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(UniqueLogin uniqueLogin) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (userRepository == null) {
            return true;
        }
        return userRepository.findOneByLogin(s) == null;
    }
}
