package ua.pp.kaeltas.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by kaeltas on 06.02.15.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(
        validatedBy = {UniqueLoginValidator.class}
)
public @interface UniqueLogin {
    String message() default "{ua.pp.kaeltas.annotations.UniqueLogin.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
