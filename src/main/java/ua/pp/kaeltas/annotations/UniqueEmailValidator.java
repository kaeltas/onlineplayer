package ua.pp.kaeltas.annotations;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import ua.pp.kaeltas.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by kaeltas on 06.02.15.
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(UniqueEmail uniqueEmail) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
//        System.out.println("--->UniqueEmailValidator.IsValid");
//        System.out.println("--->UniqueEmailValidator.IsValid1 " + userRepository);
        if (userRepository == null) {
            return true;
        }
//        System.out.println("--->UniqueEmailValidator.IsValid2 " + userRepository);
//        System.out.println("--->UniqueEmailValidator.IsValid3 " + userRepository.findOneByEmail(s));
        return userRepository.findOneByEmail(s) == null;
    }
}
