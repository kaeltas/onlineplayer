package ua.pp.kaeltas.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by kaeltas on 09.02.15.
 */
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(IllegalArgumentException.class)
    public String handleDefaultException() {
        return "redirect:/index";
    }
}
