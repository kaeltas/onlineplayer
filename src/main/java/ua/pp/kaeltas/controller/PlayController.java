package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.entity.VkGenre;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.repository.VkGenreRepository;
import ua.pp.kaeltas.service.PlaylistService;
import ua.pp.kaeltas.service.UserService;
import ua.pp.kaeltas.service.VkGenreService;
import ua.pp.kaeltas.service.VkService;

/**
 * Created by kaeltas on 30.01.15.
 */
@Controller
public class PlayController {
    @Autowired
    private UserService userService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private VkService vkService;

    @Autowired
    private VkGenreService vkGenreService;

    @RequestMapping("/play/{login}/{id}")
    public String play(Model model, @PathVariable String login, @PathVariable int id) {
        User user = null;
        if (!login.equals("popular")) {
            try {
                user = userService.findOneWithPlaylistByLogin(login);
            } catch (Exception e) {
                model.addAttribute("error", "Error while processing VkAPI request! Please, try again later! We will fix it soon!");
            }
        }

        model.addAttribute("user", user);
        //if (playlistService.findOne(id) != null) {
        model.addAttribute("playPlaylist", playlistService.findOneWithTracks(playlistService.findOne(id)));
        //}
        model.addAttribute("popularPlaylists", vkGenreService.getPopularPlaylists());

        return "play";
    }
}
