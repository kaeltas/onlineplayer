package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.SystemOption;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.service.*;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Principal;

/**
 * Created by kaeltas on 25.01.15.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private IndexController indexController;

    @Autowired
    private VkGenreService vkGenreService;

    @RequestMapping("/users")
    public String users(Model model) {
        model.addAttribute("users", userService.findAll());
        return "users";
    }

    @RequestMapping("/users/{id}")
    public String userDetail(Model model, @PathVariable int id) {
        try {
            model.addAttribute("user", userService.findOneWithPlaylist(userService.findOne(id)));
        } catch (Exception e) {
            model.addAttribute("error", "Error while processing VkAPI request! Please, try again later! We will fix it soon!");
        }
        return "user-detail";
    }

    @ModelAttribute("user")
    public User createUser() {
        return new User();
    }

    @RequestMapping("/signup")
    public String showSignUp() {
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String doSignUp(@Valid @ModelAttribute User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }
        userService.save(user);
        return "redirect:/signup?success=true";
    }

    @RequestMapping(value = "/signup/isloginavailable", method = RequestMethod.GET)
    @ResponseBody
    public String isLoginAvailable(@RequestParam String login) {
        Boolean res = userService.findOneByLogin(login) == null;
        return res.toString();
    }

    @RequestMapping(value = "/signup/isemailavailable", method = RequestMethod.GET)
    @ResponseBody
    public String isEmailAvailable(@RequestParam String email) {
        Boolean res = userService.findOneByEmail(email) == null;
        return res.toString();
    }

    @RequestMapping("/signin")
    public String signin(Model model, Principal principal) {
        model.addAttribute("authorize", true);
        return indexController.index(model, principal);
    }

    @RequestMapping("/account")
    public String account(Model model, Principal principal) {
        User user = null;
        try {
            user = userService.findOneWithPlaylistByLogin(principal.getName());
        } catch (Exception e) {
            model.addAttribute("error", "Error while processing VkAPI request! Please, try again later! We will fix it soon!");
        }
        model.addAttribute("user", user);
        model.addAttribute("popularPlaylists", vkGenreService.getPopularPlaylists());

        return "account";
    }

}
