package ua.pp.kaeltas.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ua.pp.kaeltas.service.SystemOptionService;
import ua.pp.kaeltas.spring.view.streaming.StreamingViewRenderer;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.service.TrackService;
import ua.pp.kaeltas.service.VkService;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by kaeltas on 30.01.15.
 */
@Controller
public class VkController {

    @Autowired
    private VkService vkService;

    @Autowired
    private StreamingViewRenderer streamingViewRenderer;

    @Autowired
    private TrackService trackService;

    @Autowired
    private SystemOptionService systemOptionService;

    @Autowired
    private VkService vkServise;

    @RequestMapping(value = "/vksearch", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String vkSearch(@RequestParam String q, @RequestParam int count, @RequestParam int offset) {
        try {
            return vkService.doVkSearch(q, count, offset);
        } catch (Exception e) {
            return "{\"error\":\"Error occured while searching on vk.com. Please, try again later.\"}";
        }
    }

    @RequestMapping(value = "/getaudio/{ownerId}_{audioId}")
    public ModelAndView getAudioFromVkBridge(@PathVariable int ownerId, @PathVariable int audioId) {
        try {
            ModelAndView mav = new ModelAndView(streamingViewRenderer);
            //ModelAndView mav = new ModelAndView("partialContentView");
            //FileSystemResource fileSystemResource = new FileSystemResource(new File("./src/main/webapp/WEB-INF/css/1.mp3"));

            Track track = trackService.checkRestricted(
                    trackService.findOneByOwnerIdAndAudioId(ownerId, audioId)
            );

            vkServise.vkReplaceSingleBadAudioUrl(track);

            //test error handling
            //if (true) throw new VkApiRefreshTokenException();

            URL url = new URL(track.getUrl());
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            //mav.addObject("inputStream", fileSystemResource.getInputStream());
            mav.addObject("inputStream", connection.getInputStream());
            mav.addObject("contentType", "audio/mpeg");
            mav.addObject("contentLength", Long.valueOf(connection.getContentLengthLong()));
            mav.addObject("filename", track.getOwnerId()+"_"+track.getAudioId());
            mav.addObject("lastModified", new Date());

            return mav;
        } catch (Exception e) {
//            ModelAndView mav = new ModelAndView("index");
//            mav.addObject("error", "");
//            return mav;

            //do nothing
        }
        return null;
    }


    //this method will be called only by admin. Restricted in SecurityConfig
    @RequestMapping("/getpopular")
    public String getPopular (Model model) {
        try {
            vkService.doGetPopular();
        } catch (Exception e) {
            model.addAttribute("error", "Error: " + e.getMessage());
        }
        return "redirect:/index";
    }

}
