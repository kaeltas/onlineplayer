package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.service.PlaylistService;
import ua.pp.kaeltas.service.UserService;
import ua.pp.kaeltas.service.VkGenreService;
import ua.pp.kaeltas.service.VkService;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.util.List;
import java.util.Random;


@Controller
public class IndexController {

    @Autowired
    private VkService vkService;

    @Autowired
    private VkGenreService vkGenreService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private UserService userService;

    @RequestMapping({"/", "/index"})
    public String index(Model model, Principal principal) {
        //if authorised - get User with Playlists
        if (principal != null) {
            User user = null;
            try {
                user = userService.findOneWithPlaylistByLogin(principal.getName());
            } catch (Exception e) {
                model.addAttribute("error", "Error in VkAPI request! Please, try again later!");
            }
            model.addAttribute("user", user);
        }

        List<Playlist> popularPlaylists = vkGenreService.getPopularPlaylists();
        model.addAttribute("popularPlaylists", popularPlaylists);

        System.out.println("---->"+popularPlaylists.size());

        Playlist rndPlaylist = popularPlaylists.get(new Random().nextInt(popularPlaylists.size()));
        model.addAttribute("playPlaylist", playlistService.findOneWithTracks(rndPlaylist));

        return "index";
    }

}
