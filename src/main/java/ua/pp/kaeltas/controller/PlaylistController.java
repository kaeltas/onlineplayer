package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.service.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by kaeltas on 30.01.15.
 */
@Controller
public class PlaylistController {

    @Autowired
    private UserService userService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private TrackService trackService;

    @ModelAttribute("playlist")
    public Playlist createPlaylist() {
        return new Playlist();
    }

    @ModelAttribute("track")
    public Track createTrack() {
        return new Track();
    }

    @Autowired
    private VkService vkService;

    @Autowired
    private VkGenreService vkGenreService;

    @RequestMapping("/playlist")
    public String playlistAdd(Model model, Principal principal) {
        return playlistWithIdAdd(0, model, principal);
    }

    @RequestMapping("/playlist/{viewId}")
    public String playlistWithIdAdd(@PathVariable int viewId, Model model, Principal principal) {
        //get User with Playlists with Tracks (eager)
        User user = null;
        try {
            user = userService.findOneWithPlaylistByLogin(principal.getName());
        } catch (Exception e) {
            model.addAttribute("error", "Error while processing VkAPI request! Please, try again later! We will fix it soon!");
        }

        model.addAttribute("user", user);
        model.addAttribute("activePlaylist", viewId);

        model.addAttribute("popularPlaylists", vkGenreService.getPopularPlaylists());

        return "playlist";
    }

    @RequestMapping(value = "/playlist", method = RequestMethod.POST)
    public String doAddPlaylist(@Valid @ModelAttribute Playlist playlist, BindingResult bindingResult, Model model, Principal principal) {
        return doAddPlaylistWithId(0, playlist, bindingResult, model, principal);
    }

    @RequestMapping(value = "/playlist/{viewId}", method = RequestMethod.POST)
    public String doAddPlaylistWithId(@PathVariable int viewId, @Valid @ModelAttribute Playlist playlist, BindingResult bindingResult, Model model, Principal principal) {
        if (bindingResult.hasErrors()) {
            return playlistWithIdAdd(viewId, model, principal);
        }
        playlistService.save(playlist, principal.getName());
        return "redirect:/playlist/"+playlist.getId();
    }

    @RequestMapping(value = "/playlist/track/add", method = RequestMethod.POST)
    @ResponseBody
    public String doAddTrack(@ModelAttribute Track track) {
        trackService.save(track);
        return "ok";
    }

    @RequestMapping("/playlist/{viewId}/track/delete/{id}")
    public String deleteTrack(@PathVariable int viewId, @PathVariable int id) {
        Track track = trackService.findOne(id);
        trackService.delete(track);
        return "redirect:/playlist/"+viewId;
    }

    @RequestMapping("/playlist/delete/{id}")
    public String deletePlaylist(@PathVariable Integer id) {
        Playlist playlist = playlistService.findOne(id);
        playlistService.delete(playlist);
        return "redirect:/playlist";
    }

    @RequestMapping(value = "/playlist/delete", method = RequestMethod.POST)
    public String doDeletePlaylist(@RequestParam("playlistDeleteId") Integer id) {
        playlistService.delete(playlistService.findOne(id));
        return "redirect:/playlist";
    }

    @RequestMapping(value = "/playlist/edit/{id}", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    @ResponseBody
    public String getEditPlaylistInfo(@PathVariable int id) {
        Playlist playlist = playlistService.findOne(id);
        playlist = playlistService.checkRestricted(playlist);

        return "{"
                + "\"title\":\""+playlist.getTitle() + "\""
                + ", \"description\":\"" + playlist.getDescription() + "\""
                + ", \"isPrivate\":" + playlist.getIsPrivate()
        +"}";
    }

    @RequestMapping(value = "/playlist/edit/{id}", method = RequestMethod.POST)
    public String editPlaylist(@Valid @ModelAttribute Playlist playlist, BindingResult bindingResult, @PathVariable int id, Model model, Principal principal) {
        if (bindingResult.hasErrors()) {
            return playlistWithIdAdd(id, model, principal);//"redirect:/playlist/"+id;
        }
        Playlist oldPlaylist = playlistService.findOne(id);
        playlistService.update(oldPlaylist, playlist);

        return "redirect:/playlist/"+id;
    }



}
