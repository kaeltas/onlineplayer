package ua.pp.kaeltas.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.SystemOption;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.VkGenre;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.json.vk.AccessToken;
import ua.pp.kaeltas.json.vk.audio.AudioTracksRoot;
import ua.pp.kaeltas.json.vk.audio.Response;
import ua.pp.kaeltas.json.vk.error.ErrorRoot;
import ua.pp.kaeltas.repository.PlaylistRepository;
import ua.pp.kaeltas.repository.TrackRepository;
import ua.pp.kaeltas.repository.VkGenreRepository;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import java.io.*;
import java.net.*;
import java.security.GeneralSecurityException;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 * Created by kaeltas on 29.01.15.
 */
@Service
@Transactional
public class VkService {

    @Autowired
    private SystemOptionService systemOptionService;

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    PlaylistRepository playlistRepository;

    @Autowired
    VkGenreRepository vkGenreRepository;

    public String doVkSearch(String q, int count, int offset) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        String qEnc = q;

        try {
            qEnc = URLEncoder.encode(q, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            //do nothing
        }
        String vkSearchUrl = "https://api.vk.com/method/audio.search?q=" + qEnc
                + "&count=" + count
                + "&offset=" + offset
                + "&v=5.28";

        //test error handling
        //if (true) throw new VkApiRefreshTokenException();
        return doVkApiRequest(vkSearchUrl);
    }

    private void doGetPopular(Integer vkGenreId) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        String vkGetPopularUrl = "https://api.vk.com/method/audio.getPopular?genre_id=" + vkGenreId
                + "&count=30"
                + "&offset=0"
                + "&v=5.28";
        //System.out.println(vkGetPopularUrl);
        String resultJson = doVkApiRequest(vkGetPopularUrl);

        //System.out.println("--------->>>>" + vkGenreId);
        //System.out.println(resultJson);

        if (resultJson != null && !resultJson.isEmpty()) {
            //delete all old entries
            VkGenre genre = vkGenreRepository.findOneByVkId(vkGenreId);
            Playlist playlist = genre.getPlaylist();
            for (Track track : playlist.getTracks()) {
                trackRepository.delete(track);
            }

            //parse Json response and add entries to Popular playlist
            AudioTracksRoot tracksRoot = new Gson().fromJson(resultJson, AudioTracksRoot.class);
            for (Response r : tracksRoot.response) {
                Track track = new Track();
                track.setArtist(r.artist);
                track.setUrl(r.url);
                track.setTitle(r.title);
                track.setDuration(r.duration);
                track.setPlaylist(playlist);
                track.setAudioId(r.id);
                track.setOwnerId(r.owner_id);

                trackRepository.save(track);
                //System.out.print(countTracks++ + ", ");
            }


            playlist.setDescription("Total tracks : " + tracksRoot.response.length);
            playlist.setLastUpdateDatetime(new Date());
            playlistRepository.save(playlist);
        }
    }

    public void doGetPopular() throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        for (VkGenre vkGenre : vkGenreRepository.findAll()) {
            doGetPopular(vkGenre.getVkId());
        }
    }


    private String doVkApiRequest(String strUrl) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        String result = null;
        try {
            Integer retryCount = 3;
            while (retryCount-- > 0) {
                SystemOption vkToken = systemOptionService.findOneByName("vk_access_token");
                String strUrlWithToken = strUrl+"&access_token="+vkToken.getValue();

                result = readAllDataFromStream(new URL(strUrlWithToken).openStream());
                //Check, if we have got error from vk
                if (result.startsWith("{\"error\":")) {
                    System.out.println("VkError: [" + result + "]");
                    handleVkApiError(result);
                    continue;
                } else { // OK
                    return result;
                }
            }
        } catch (IOException e) {
            throw new VkApiCannotReadDataFromUrlException("Error while reading data from url", e);
        }

        return null;
    }

    private void handleVkApiError(String error) throws VkApiRefreshTokenException {
        Gson gson = new GsonBuilder().create();
        ErrorRoot jsonError = gson.fromJson(error, ErrorRoot.class);

        switch (jsonError.error.error_code) {

            //Wrong access token. Lets refresh it.
            case 5:
            //Internal server error. Probably because of wrong token.
            case 10: {
                //System.out.println("Wrong token. Refreshing it.");
                try {
                    doRefreshVkAccessToken();
                }  catch (GeneralSecurityException | IOException e) {
                    throw new VkApiRefreshTokenException("Error refresh token", e);
                }

                break;
            }
            default: {
                break;
            }
        }
    }

    private String readAllDataFromStream(InputStream inputStream) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        }
    }

    private void doRefreshVkAccessToken() throws IOException, GeneralSecurityException {
        //OAuth. #1. Send request to get "code", which lately would be changed to access_token.
        String code = null;

        HttpURLConnection urlConnection = (HttpURLConnection)new URL(
                "https://oauth.vk.com/authorize?client_id="+systemOptionService.findOneByName("vk_client_id").getValue()
                        +"&scope=8"
                        +"&redirect_uri="+systemOptionService.findOneByName("vk_redirect_url").getValue()
                        +"&response_type=code"
                        +"&v=5.28"
                        +"&state=\"FIRST_STATE\"").openConnection();
        urlConnection.connect();

        //We received some data. Actually, we have been redirected to vk login page.
        if (urlConnection.getContentLength() > 0 && urlConnection.getHeaderField("Location") == null) {
            String responseData = readAllDataFromStream(urlConnection.getInputStream());

            //parse html <form> parameters to singnin in vk.com
            if (responseData.contains("action=\"https://login.vk.com/?act=login")) {
                // RegEx pattern to get form-action : "<form method=\"post\" action=\"(.+?)\">"
                // RegEx pattern to get hidden-fields : "<input type=\"hidden\" name=\"(.+?)\" value=\"(.+?)\""
                Matcher matcherFormAction = Pattern
                        .compile("<form method=\"post\" action=\"(.+?)\">")
                        .matcher(responseData);
                Matcher matcherHiddenFields = Pattern
                        .compile("<input type=\"hidden\" name=\"(.+?)\" value=\"(.+?)\"")
                        .matcher(responseData);

                String formAction = null;
                if (matcherFormAction.find()) {
                    formAction = matcherFormAction.group(1);
                }

                StringBuilder sbEncodedData = new StringBuilder();
                while (matcherHiddenFields.find()) {
                    sbEncodedData.append(
                            URLEncoder.encode(matcherHiddenFields.group(1), "UTF-8")
                                    +"="
                                    +URLEncoder.encode(matcherHiddenFields.group(2), "UTF-8")
                                    + "&"
                    );
                    //System.out.println(matcherHiddenFields.group(1)+"="+matcherHiddenFields.group(2));
                }


                byte[] encodedKey = new Base64().decode("yK2MihODLws=");
                SecretKey secretKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "DES");
                Cipher desCipher = Cipher.getInstance("DES");
                desCipher.init(Cipher.DECRYPT_MODE, secretKey);



                byte[] byteDecryptedEmail = desCipher.doFinal(
                        new Base64().decode(
                                systemOptionService.findOneByName("vk_email").getValue())
                );
                byte[] byteDecryptedPass = desCipher.doFinal(
                        new Base64().decode(
                                systemOptionService.findOneByName("vk_pass").getValue())
                );

                sbEncodedData.append("email="+URLEncoder.encode(new String(byteDecryptedEmail), "UTF-8")+"&");
                sbEncodedData.append("pass="+URLEncoder.encode(new String(byteDecryptedPass), "UTF-8")+"&");

                String type = "application/x-www-form-urlencoded";
                String encodedData = sbEncodedData.toString();
                //do login to vk.com. Send parameters via POST request.
                URL u = new URL(formAction);
                HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", type);
                conn.setRequestProperty( "Content-Length", String.valueOf(encodedData.length()));
                try (OutputStream os = conn.getOutputStream()) {
                    os.write(encodedData.getBytes());
                    os.flush();
                }

                //Storage for all received cookies
                Map<String, String> cookiesMap = new HashMap<>();

                //Get cookies from response
                StringBuilder sbCookies = new StringBuilder();
                Pattern cookiePattern = Pattern.compile("(.+?)=(.+?);");
                for (String cookie : conn.getHeaderFields().get("Set-Cookie")) {
                    Matcher matcher;
                    if ((matcher = cookiePattern.matcher(cookie)).find()) {
                        cookiesMap.put(matcher.group(1), matcher.group(2));
                        sbCookies.append(matcher.group(1) + "=" + matcher.group(2) + ";");
                    }
                }

                //Handle some redirects, which vk do after singin
                String location = conn.getHeaderField("Location");
                Integer maxRedirectCount = 10;
                while (maxRedirectCount-- > 0) {
                    URL u1 = new URL(location);
                    //System.out.println("---->location1: ["+location+"]");
                    HttpURLConnection conn1 = (HttpURLConnection) u1.openConnection();
                    conn1.setInstanceFollowRedirects(false);
                    conn1.setRequestProperty("Cookie", sbCookies.toString());
                    conn1.connect();

                    //Get cookies from response
                    if (conn1.getHeaderFields().get("Set-Cookie") != null) {
                        for (String cookie : conn1.getHeaderFields().get("Set-Cookie")) {
                            Matcher matcher;
                            if ((matcher = cookiePattern.matcher(cookie)).find()) {
                                cookiesMap.put(matcher.group(1), matcher.group(2));
                            }
                        }
                    }

                    sbCookies = new StringBuilder();
                    for (Map.Entry<String, String> cookie : cookiesMap.entrySet()) {
                        sbCookies.append(cookie.getKey() + "=" + cookie.getValue() + "; ");
                    }

                    try {
                        String response = readAllDataFromStream(conn1.getInputStream());
                        //System.out.println("---->conn1.IS: [" + response + "]");
                        //confirm vk login from "strange location"
                        if (response.contains("action=\"/login.php?act=security_check")) {
                            Matcher matcherFormAction2 = Pattern
                                    .compile("<form method=\"post\" action=\"(.+?)\">")
                                    .matcher(response);
                            formAction = null;
                            if (matcherFormAction2.find()) {
                                formAction = matcherFormAction2.group(1);
                            }

                            String type2 = "application/x-www-form-urlencoded";
                            String encodedData2 = "code="+URLEncoder.encode(systemOptionService.findOneByName("vk_code").getValue(), "UTF-8");
                            //do login to vk.com. Send parameters via POST request.
                            URL u2 = new URL(conn1.getURL().getProtocol(), conn1.getURL().getHost(), formAction);
                            conn1 = (HttpURLConnection) u2.openConnection();
                            conn1.setInstanceFollowRedirects(false);
                            conn1.setDoOutput(true);
                            conn1.setRequestMethod("POST");
                            conn1.setRequestProperty("Cookie", sbCookies.toString());
                            conn1.setRequestProperty("Content-Type", type2);
                            conn1.setRequestProperty( "Content-Length", String.valueOf(encodedData2.length()));
                            try (OutputStream os = conn1.getOutputStream()) {
                                os.write(encodedData2.getBytes());
                                os.flush();
                            }

                            //Get cookies from response
                            for (String cookie : conn.getHeaderFields().get("Set-Cookie")) {
                                Matcher matcher;
                                if ((matcher = cookiePattern.matcher(cookie)).find()) {
                                    cookiesMap.put(matcher.group(1), matcher.group(2));
                                }
                            }
                            sbCookies = new StringBuilder();
                            for (Map.Entry<String, String> cookie : cookiesMap.entrySet()) {
                                sbCookies.append(cookie.getKey() + "=" + cookie.getValue() + "; ");
                            }

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    location = conn1.getHeaderField("Location");
                    //System.out.println("---->location2: ["+location+"]");
                    if (location.startsWith(systemOptionService.findOneByName("vk_redirect_url").getValue()) || conn1.getResponseCode() == 200) {
                        //System.out.println("finish");
                        //System.out.println("location=" + location);

                        Matcher matcher = Pattern.compile("code=([\\w\\d]+?)&").matcher(location);
                        if (matcher.find()) {
                            code = matcher.group(1);
                        }

                        break;
                    }

                }

                //System.out.println("code="+code);

                //OAuth. #2. Get access_token.
                //If we successfully receive "code"
                if (code != null) {

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        //do nothing
                    }

                    URL getAccessTokenUrl = new URL("https://oauth.vk.com/access_token?"
                            +"client_id="+systemOptionService.findOneByName("vk_client_id").getValue()
                            +"&client_secret="+systemOptionService.findOneByName("vk_client_secret").getValue()
                            +"&code="+code
                            +"&redirect_uri="+systemOptionService.findOneByName("vk_redirect_url").getValue());

                    String jsonAccessToken = readAllDataFromStream(getAccessTokenUrl.openStream());

                    AccessToken accessToken = new Gson().fromJson(jsonAccessToken, AccessToken.class);
//                    System.out.println(accessToken.access_token);
//                    System.out.println(accessToken.expires_in);

                    SystemOption vk_access_token = systemOptionService.findOneByName("vk_access_token");
                    vk_access_token.setValue(accessToken.access_token);
                    systemOptionService.save(vk_access_token);

                    SystemOption vk_expires_in = systemOptionService.findOneByName("vk_expires_in");
                    vk_expires_in.setValue(accessToken.expires_in);
                    systemOptionService.save(vk_expires_in);

                    SystemOption vk_access_token_get_time = systemOptionService.findOneByName("vk_access_token_get_time");
                    vk_access_token_get_time.setValue(String.valueOf(new Date().getTime()));
                    systemOptionService.save(vk_access_token_get_time);

                }
            }
            //System.out.println(responseData);
            //System.out.println();
        }
    }

    public String vkReplaceSingleBadAudioUrl(Track track) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        if (isBadUrl(track.getUrl())) {
            String jsonString = vkGetAudioUrlsById(Arrays.asList(new Track[]{track}));

            Gson gson = new GsonBuilder().create();
            AudioTracksRoot audioTracksRoot = gson.fromJson(jsonString, AudioTracksRoot.class);

            if (audioTracksRoot != null && audioTracksRoot.response != null) {
                Response resp;
                try {
                    if ((resp = audioTracksRoot.findByOwnerIdAndId(track.getOwnerId(), track.getAudioId())) != null) {
                        track.setArtist(resp.artist);
                        track.setDuration(resp.duration);
                        track.setTitle(resp.title);
                        track.setUrl(resp.url);
                        trackRepository.save(track);
                    }
                } catch (Exception e) {
                    trackRepository.delete(track);
                    //track.getPlaylist().getTracks().remove(track);
                }
            }
        }

        return null;
    }

    public String vkReplaceBadAudioUrlsInSinglePlaylist(Playlist playlist) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        List<Track> badUrlTracks = new ArrayList<>();
        for (Track track : playlist.getTracks()) {
            if (isBadUrl(track.getUrl())) {
                badUrlTracks.add(track);
            }
        }

        //test error handling
        //if (true) throw new VkApiRefreshTokenException();

        if (badUrlTracks.size() > 0) {
            String jsonString = vkGetAudioUrlsById(badUrlTracks);

            Gson gson = new GsonBuilder().create();
            AudioTracksRoot audioTracksRoot = gson.fromJson(jsonString, AudioTracksRoot.class);

            if (audioTracksRoot != null && audioTracksRoot.response != null) {
                for (Track track : badUrlTracks) {
                    Response resp;
                    try {
                        if ((resp = audioTracksRoot.findByOwnerIdAndId(track.getOwnerId(), track.getAudioId())) != null) {
                            track.setArtist(resp.artist);
                            track.setDuration(resp.duration);
                            track.setTitle(resp.title);
                            track.setUrl(resp.url);
                            trackRepository.save(track);
                        }
                    } catch (Exception e) {
                        trackRepository.delete(track);
                        playlist.getTracks().remove(track);
                    }
                }
            }
        }

        return null;
    }

    public String vkReplaceBadAudioUrlsById(List<Playlist> playlists) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        for (Playlist playlist : playlists) {
            vkReplaceBadAudioUrlsInSinglePlaylist(playlist);
        }
        return null;
    }

    private Boolean isBadUrl(String url) {
        Boolean result = true;
        if (url != null && url.length() > 0) {
            HttpURLConnection connection = null;
            try {
                URL testUrl = new URL(url.replace("\\/", "/"));
                connection = (HttpURLConnection) testUrl.openConnection();
                if (connection.getResponseCode() == 200) {
                    result = false;
                }
            } catch (IOException e) {
                //do nothing
                //e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        return result;
    }

    private String vkGetAudioUrlsById(List<Track> tracks) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        StringBuilder csvString = new StringBuilder();
        Iterator<Track> iterator = tracks.iterator();
        while (iterator.hasNext()) {
            Track track = iterator.next();
            csvString.append(track.getOwnerId()+"_"+track.getAudioId());
            if (iterator.hasNext()) {
                csvString.append(",");
            }
        }

        String vkAudioGetUrl = "https://api.vk.com/method/audio.getById?audios="
                +csvString.toString()
                +"&v=5.28";
        return doVkApiRequest(vkAudioGetUrl);
    }

}
