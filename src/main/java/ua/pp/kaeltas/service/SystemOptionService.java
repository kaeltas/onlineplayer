package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.SystemOption;
import ua.pp.kaeltas.repository.SystemOptionRepository;

/**
 * Created by kaeltas on 28.01.15.
 */
@Service
public class SystemOptionService {

    @Autowired
    private SystemOptionRepository systemOptionRepository;

    public void save(SystemOption systemOption) {
        systemOptionRepository.save(systemOption);
    }

    public SystemOption findOneByName(String name) {
        return systemOptionRepository.findOneByName(name);
    }


}
