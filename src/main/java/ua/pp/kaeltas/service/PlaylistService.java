package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.repository.PlaylistRepository;
import ua.pp.kaeltas.repository.TrackRepository;
import ua.pp.kaeltas.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaeltas on 26.01.15.
 */
@Service
@Transactional
public class PlaylistService {

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TrackRepository trackRepository;

    public void save(Playlist playlist, String login) {
        User user = userRepository.findOneByLogin(login);
        playlist.setUser(user);
        playlistRepository.save(playlist);
        playlistRepository.flush();
    }

    public void save(Playlist playlist) {
        playlistRepository.save(playlist);
        playlistRepository.flush();
    }

    public Playlist findOne(Integer id) {
        return playlistRepository.findOne(id);
    }

    @PreAuthorize("#playlist.user.login == authentication.name or hasRole('ROLE_ADMIN')")
    public void delete(@P("playlist") Playlist playlist) {
        playlistRepository.delete(playlist);
    }

    @PreAuthorize("#p.isPrivate == false or #p.user.login == authentication.name")
    public Playlist findOneWithTracks(@P("p") Playlist playlist) {
        List<Track> tracks = trackRepository.findByPlaylist(playlist);
        playlist.setTracks(tracks);

        return playlist;
    }


    @PostFilter("filterObject.isPrivate == false or filterObject.user.login == authentication.name")
    public List<Playlist> findAllByUser(User user) {
        return playlistRepository.findByUser(user);
    }

    @PreAuthorize("#p.user.login == authentication.name or hasRole('ROLE_ADMIN')")
    public void update(@P("p")Playlist playlist, Playlist editedPlaylist) {
        playlist.setTitle(editedPlaylist.getTitle());
        playlist.setDescription(editedPlaylist.getDescription());
        playlist.setIsPrivate(editedPlaylist.getIsPrivate());

        playlistRepository.save(playlist);
        playlistRepository.flush();
    }

    @PreAuthorize("#p.user.login == authentication.name")
    public Playlist checkRestricted(@P("p")Playlist playlist) {
        return playlist;
    }
}
