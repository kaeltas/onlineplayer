package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Role;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kaeltas on 26.01.15.
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userRepository.findOneByLogin(login);

        StringBuilder roles = new StringBuilder();
        Iterator<Role> iter = user.getRoles().iterator();
        while (iter.hasNext()) {
            Role role = iter.next();
            roles.append(role.getName());
            if (iter.hasNext()) {
                roles.append(",");
            }
        }
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(roles.toString());

        // #org.springframework.security.core.userdetails.User should be returned from this method
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(login, user.getHashedPassword(), true, true,
                true, true, grantedAuthorities );
        return userDetails;
    }
}
