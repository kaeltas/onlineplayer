package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.User;
import ua.pp.kaeltas.exception.VkApiCannotReadDataFromUrlException;
import ua.pp.kaeltas.exception.VkApiRefreshTokenException;
import ua.pp.kaeltas.repository.TrackRepository;
import ua.pp.kaeltas.repository.PlaylistRepository;
import ua.pp.kaeltas.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaeltas on 25.01.15.
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    PlaylistService playlistService;

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private VkService vkService;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(int id) {
        return userRepository.findOne(id);
    }


    public User findOneWithPlaylist(User user) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        List<Playlist> playlistsWithTracks = new ArrayList<>();
        List<Playlist> playlists = playlistService.findAllByUser(user);
        for (Playlist playlist : playlists) {
            //Check bad urls. Apply it only to user-playlists (popular playlists - are excluded).
            vkService.vkReplaceBadAudioUrlsInSinglePlaylist(playlist);
            playlistsWithTracks.add(playlistService.findOneWithTracks(playlist));
        }

        user.setPlaylists(playlistsWithTracks);

        return user;
    }

    public User findOneWithPlaylistWoTracks(User user) {
        List<Playlist> playlistsWithTracks = new ArrayList<>();
        List<Playlist> playlists = playlistService.findAllByUser(user);
        for (Playlist playlist : playlists) {
            playlistsWithTracks.add(playlistService.findOneWithTracks(playlist));
        }

        user.setPlaylists(playlistsWithTracks);

        return user;
    }

    public void save(User user) {
        //encode password
        user.setHashedPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userRepository.save(user);

        //Create first playlist for user
        Playlist playlist = new Playlist();
        playlist.setTitle("My first playlist");
        playlist.setUser(user);
        playlist.setDescription("Automatically created playlist");
        playlistService.save(playlist);

    }

    public User findOneWithPlaylistByLogin(String login) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        User user = userRepository.findOneByLogin(login);
        return findOneWithPlaylist(user);
    }

    public User findOneByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }

    public User findOneByEmail(String email) {
        return userRepository.findOneByEmail(email);
    }

    /*public User findOneWithPlaylistWoTracksByLogin(String login) {
        User user = userRepository.findOneByLogin(login);
        return findOneWithPlaylistWoTracks(user);
    }

    public User findOneWithPlaylistByUser(User user) throws VkApiCannotReadDataFromUrlException, VkApiRefreshTokenException {
        return findOneWithPlaylist(user);
    }*/

}
