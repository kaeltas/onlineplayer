package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.entity.VkGenre;
import ua.pp.kaeltas.repository.TrackRepository;
import ua.pp.kaeltas.repository.VkGenreRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaeltas on 02.02.15.
 */
@Service
public class VkGenreService {

    @Autowired
    VkGenreRepository vkGenreRepository;

    @Autowired
    TrackRepository trackRepository;

    public List<Playlist> getPopularPlaylists() {
        List<Playlist> playlists = new ArrayList<>();

        for (VkGenre genre : vkGenreRepository.findAll()) {

            Playlist playlist = genre.getPlaylist();

            //List<Track> tracks = trackRepository.findByPlaylist(playlist);
            //playlist.setTracks(tracks);

            if (trackRepository.countByPlaylist(playlist) > 0) {
                playlists.add(playlist);
            }
        }

        return playlists;
    }

}
