package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.*;
import ua.pp.kaeltas.repository.*;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by kaeltas on 25.01.15.
 */

@Transactional
@Service
public class InitDbService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private SystemOptionRepository systemOptionRepository;

    @PostConstruct
    public void initDb() {

        Role roleAdmin = new Role();
        roleAdmin.setName("ROLE_ADMIN");
        roleRepository.save(roleAdmin);

        Role roleUser = new Role();
        roleUser.setName("ROLE_USER");
        roleRepository.save(roleUser);

        User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setEmail("admin@foo.bar");
        userAdmin.setPassword(new BCryptPasswordEncoder().encode("admin"));
        userAdmin.setRoles(Arrays.asList(new Role[] {roleAdmin, roleUser}));
        userRepository.save(userAdmin);

        User userUser = new User();
        userUser.setLogin("user");
        userUser.setEmail("user@foo.bar");
        userUser.setPassword(new BCryptPasswordEncoder().encode("user"));
        userUser.setRoles(Arrays.asList(new Role[] {roleUser}));
        userRepository.save(userUser);

        Playlist playlist = new Playlist();
        playlist.setTitle("first playlist");
        playlist.setDescription("first playlist description");
        playlist.setUser(userAdmin);
        //playlist.setTracks();
        playlistRepository.save(playlist);

        Track track1 = new Track();
        track1.setTitle("I Want It All");
        //track1.setArtist("Queen");
        track1.setAudioId(73833540);
        track1.setOwnerId(14011332);
        //track1.setDuration(241);
        //track1.setUrl("http:\\/\\/cs7-4v4.vk-cdn.net\\/p24\\/b10fbe03f6d31d.mp3?extra=Apy0j-u7n4e6ICnAZ8Ogmmsn-8PNAkeBKxoXL91xpVNU6DZJimbA-bEINEjuuEfxYGqRG3ocNjoI3vaZefmiBEzDTfdnlQy9");
        track1.setUrl("http:\\/\\/cs7-4v4.vk-cdn.net\\/p24\\/22222222b10fbe03f6d31d.mp3?extra=Apy0j-u7n4e6ICnAZ8Ogmmsn-8PNAkeBKxoXL91xpVNU6DZJimbA-bEINEjuuEfxYGqRG3ocNjoI3vaZefmiBEzDTfdnlQy9");
        track1.setPlaylist(playlist);
        trackRepository.save(track1);

        Track track2 = new Track();
        track2.setTitle("Friends will be Friends");
        track2.setArtist("Queen");
        track2.setAudioId(93275062);
        track2.setOwnerId(480978);
        //track2.setDuration(244);
        //track2.setUrl("http:\\/\\/cs7-5v4.vk-cdn.net\\/p10\\/9a57d9acbc51d3.mp3?extra=wf22JXKbNYkKwe4mpBGi2rOjufBqiUmxAkMprUwgSLOGSs_Q_YJK1hRbD7a_Ps5CAHbyKFquuPoimgbQ9HPAbfpMiqbb5A");
        track2.setPlaylist(playlist);
        trackRepository.save(track2);

        Playlist playlist2 = new Playlist();
        playlist2.setTitle("second playlist");
        playlist2.setDescription("second playlist description");
        playlist2.setUser(userAdmin);
        //playlist.setTracks();
        playlistRepository.save(playlist2);

        Track track21 = new Track();
        track21.setTitle("We Will Rock You");
        track21.setArtist("Queen");
        track21.setAudioId(90016928);
        track21.setOwnerId(14246168);
        //track21.setDuration(122);
        track21.setUrl("http:\\/\\/cs7-1v4.vk-cdn.net\\/p5\\/54a4282a13349e.mp3?extra=GFuvFlw4Hmujjj9llMZ_1iFvQ0LCWY8brDo_rIpCApgiDRHVqKoFhZhSxvnbzpWI-nBbwZH_w-v87UoUuLVfAIt2gLqO1HYQ");
        track21.setPlaylist(playlist2);
        trackRepository.save(track21);

        Track track22 = new Track();
        track22.setTitle("audio22");
        track22.setUrl("http://foo.bar");
        track22.setPlaylist(playlist2);
        trackRepository.save(track22);


        //System options
        SystemOption systemOption = new SystemOption();
        systemOption.setName("vk_access_token");
        systemOption.setValue("df306f4ff35c6d36583114e8f88f26e1b590295ff39049d4732988c694b945924c315909118e08272ee4b");
        systemOptionRepository.save(systemOption);

        systemOption = new SystemOption();
        systemOption.setName("vk_access_token_expires");
        systemOption.setValue("86266");
        systemOptionRepository.save(systemOption);

        systemOption = new SystemOption();
        systemOption.setName("vk_access_token_get_time");
        systemOption.setValue(String.valueOf(new Date().getTime()));
        systemOptionRepository.save(systemOption);

        systemOption = new SystemOption();
        systemOption.setName("vk_client_id");
        systemOption.setValue("4750560");
        systemOptionRepository.save(systemOption);

        systemOption = new SystemOption();
        systemOption.setName("vk_client_secret");
        systemOption.setValue("3OIXKwW8Lxzv16hMIwBr");
        systemOptionRepository.save(systemOption);


    }
}
