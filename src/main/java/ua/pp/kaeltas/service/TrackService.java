package ua.pp.kaeltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;
import ua.pp.kaeltas.repository.PlaylistRepository;
import ua.pp.kaeltas.repository.TrackRepository;

/**
 * Created by kaeltas on 28.01.15.
 */
@Service
public class TrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    public void save(Track track) {
        Playlist playlist = playlistRepository.findOne(track.getPlaylist().getId());
        track.setPlaylist(playlist);
        trackRepository.save(track);
    }

    public Track findOne(int id) {
        return trackRepository.findOne(id);
    }

    @PreAuthorize("#track.playlist.user.login == authentication.name")
    public void delete(@P("track") Track track) {
        trackRepository.delete(track);
    }

    public Track findOneByOwnerIdAndAudioId(int ownerId, int audioId) {
        return trackRepository.findOneByOwnerIdAndAudioId(ownerId, audioId);
    }

    @PreAuthorize("#t.playlist.isPrivate == false or #t.playlist.user.login == authentication.name")
    public Track checkRestricted(@P("t") Track track) {
        return track;
    }
}
