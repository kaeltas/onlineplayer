package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.SystemOption;

/**
 * Created by kaeltas on 28.01.15.
 */
public interface SystemOptionRepository extends JpaRepository<SystemOption, Integer> {

    SystemOption findOneByName(String name);

}
