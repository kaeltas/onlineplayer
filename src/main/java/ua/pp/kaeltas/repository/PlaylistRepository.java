package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.User;

import java.util.List;

/**
 * Created by kaeltas on 25.01.15.
 */
public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

    List<Playlist> findByUser(User user);

}
