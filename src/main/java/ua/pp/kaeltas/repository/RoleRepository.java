package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.Role;

/**
 * Created by kaeltas on 25.01.15.
 */
public interface RoleRepository extends JpaRepository<Role, Integer>{
    Role findByName(String name);
}
