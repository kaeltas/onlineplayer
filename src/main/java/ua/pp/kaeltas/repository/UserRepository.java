package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.User;

/**
 * Created by kaeltas on 25.01.15.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    User findOneByLogin(String login);

    User findOneByEmail(String email);
}
