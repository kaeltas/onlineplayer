package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.VkGenre;

import java.util.List;

/**
 * Created by kaeltas on 02.02.15.
 */
public interface VkGenreRepository extends JpaRepository<VkGenre, Integer> {
    VkGenre findOneByVkId(Integer vkId);
}
