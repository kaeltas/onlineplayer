package ua.pp.kaeltas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.pp.kaeltas.entity.Playlist;
import ua.pp.kaeltas.entity.Track;

import java.util.List;

/**
 * Created by kaeltas on 25.01.15.
 */
public interface TrackRepository extends JpaRepository<Track, Integer> {

    List<Track> findByPlaylist(Playlist playlist);

    Track findOneByOwnerIdAndAudioId(int ownerId, int audioId);

    int countByPlaylist(Playlist playlist);
}
