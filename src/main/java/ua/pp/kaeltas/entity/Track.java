package ua.pp.kaeltas.entity;

import javax.persistence.*;

/**
 * Created by kaeltas on 25.01.15.
 */
@Entity
public class Track {

    @Id
    @GeneratedValue
    private Integer id;

    private String title;

    private String artist;

    private Integer duration;

    @Column(name = "owner_id")
    private Integer ownerId;

    @Column(name = "audio_id")
    private Integer audioId;

    @Column(length = 2048)
    private String url;

    private String ip;

    @ManyToOne
    @JoinColumn
    private Playlist playlist;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getAudioId() {
        return audioId;
    }

    public void setAudioId(Integer audioId) {
        this.audioId = audioId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }
}
