package ua.pp.kaeltas.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by kaeltas on 25.01.15.
 */
@Entity
public class Playlist {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 1024)
    @Size(min = 1, message = "Title should be at least 1 character long")
    private String title;

    @Column(length = 1024)
    private String description;

    //@Column(name = "is_private", columnDefinition = "BOOL default TRUE", nullable = false)
    @Column(name = "is_private")
    private Boolean isPrivate = true;

    @Column(name = "last_update_datetime")
    private Date lastUpdateDatetime;

    @OneToMany(mappedBy = "playlist", cascade = CascadeType.REMOVE)
    private List<Track> tracks;

    @ManyToOne
    @JoinColumn
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    public void setLastUpdateDatetime(Date updatedDatetime) {
        this.lastUpdateDatetime = updatedDatetime;
    }

    @PrePersist
    void onCreate() {
        this.setLastUpdateDatetime(new Date());
    }

    @PreUpdate
    void onPersist() {
        this.setLastUpdateDatetime(new Date());
    }

}
