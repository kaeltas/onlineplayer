package ua.pp.kaeltas.entity;

import javax.persistence.*;

/**
 * Created by kaeltas on 02.02.15.
 */
@Entity
public class VkGenre {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "vk_id")
    private int vkId;

    @Column(name = "vk_name")
    private String vkName;

    @OneToOne(cascade = CascadeType.REMOVE)
    private Playlist playlist;

    public VkGenre() {
    }

    public VkGenre(int vkId, String vkName) {
        this.vkId = vkId;
        this.vkName = vkName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getVkId() {
        return vkId;
    }

    public void setVkId(int vkId) {
        this.vkId = vkId;
    }

    public String getVkName() {
        return vkName;
    }

    public void setVkName(String vkName) {
        this.vkName = vkName;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

}
