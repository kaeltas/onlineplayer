package ua.pp.kaeltas.entity;

import org.hibernate.validator.constraints.Email;
import ua.pp.kaeltas.annotations.UniqueEmail;
import ua.pp.kaeltas.annotations.UniqueLogin;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/*
*
 * Created by kaeltas on 24.01.15.
*/

@Entity
@Table(name = "jbop_user")
public class User {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    @Size(min = 3, message = "Login must be at least 3 characters long!")
    @UniqueLogin(message = "This login has already been taken!")
    private String login;

    @Column(unique = true)
    @Size(min = 1, message = "Invalid email!")
    @Email(message = "Invalid email!")
    @UniqueEmail(message = "This email has already been taken!")
    private String email;

    @Size(min = 5, message = "Password must be at least 5 characters long!")
    @Transient
    private String password;

    private String hashedPassword;

    @ManyToMany
    @JoinTable
    private List<Role> roles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Playlist> playlists;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
}
