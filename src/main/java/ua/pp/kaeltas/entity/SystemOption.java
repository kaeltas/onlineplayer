package ua.pp.kaeltas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by kaeltas on 28.01.15.
 */
@Entity
public class SystemOption {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 1024)
    private String name;

    @Column(length = 1024)
    private String value;

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
