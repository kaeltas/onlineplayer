package ua.pp.kaeltas.json.vk;

/**
 * Created by kaeltas on 30.01.15.
 */
public class AccessToken {
    public String access_token;
    public String expires_in;
    public int user_id;
}
