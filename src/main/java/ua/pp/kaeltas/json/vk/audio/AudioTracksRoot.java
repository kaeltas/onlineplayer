package ua.pp.kaeltas.json.vk.audio;

/**
 * Created by kaeltas on 29.01.15.
 */
public class AudioTracksRoot {
    public Response [] response;

    public Response findByOwnerIdAndId(Integer ownerId, Integer id) {
        //System.out.println("Look for " + ownerId + ","+id);
        for (Response resp : response) {
            //System.out.println("  ->" + resp.owner_id + "," + resp.id);
            if (ownerId.equals(resp.owner_id) && id.equals(resp.id)) {
                //System.out.println("FOUND  ->" + resp.owner_id + "," + resp.id);
                return resp;
            }
        }
        return null;
    }
}
