package ua.pp.kaeltas.json.vk.error;

/**
 * Created by kaeltas on 30.01.15.
 */
public class Error {
    public Integer error_code;
    public String error_msg;
    public VkRequestParam[] request_params;
}
