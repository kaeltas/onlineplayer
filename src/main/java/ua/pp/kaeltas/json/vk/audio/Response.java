package ua.pp.kaeltas.json.vk.audio;

/**
 * Created by kaeltas on 29.01.15.
 */
public class Response {
    public Integer id;
    public Integer owner_id;
    public String artist;
    public String title;
    public Integer duration;
    public String url;
    public Integer lyrics_id;
    public Integer album_id;
    public Integer genre_id;

    @Override
    public String toString() {
        return "Response{" +
                "id=" + id +
                ", owner_id=" + owner_id +
                ", artist='" + artist + '\'' +
                ", title='" + title + '\'' +
                ", duration=" + duration +
                ", url='" + url + '\'' +
                ", lyrics_id=" + lyrics_id +
                ", album_id=" + album_id +
                ", genre_id=" + genre_id +
                '}';
    }
}
