package ua.pp.kaeltas.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created by kaeltas on 25.01.15.
 */
@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserDetailsService userDetailsServiceImpl;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                .withUser("user1").password("user1").roles("USER")
//                .and()
//                .withUser("admin1").password("admin1").roles("ADMIN");

        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(new BCryptPasswordEncoder());
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/signup", "/signin", "/", "/index").permitAll()
            .antMatchers("/signup/isloginavailable", "/signup/isemailavailable").permitAll()
            .antMatchers("/css/**", "/js/**").permitAll()
            .antMatchers("/play/**").permitAll()
            .antMatchers("/getaudio/**").permitAll()
            .antMatchers("/vksearch**").permitAll()
            .antMatchers("/users/**").hasRole("ADMIN")
            .antMatchers("/getpopular").hasRole("ADMIN")
            //.antMatchers("/admin/**").hasRole("ADMIN")
            //.antMatchers("/db/**").access("hasRole('ROLE_ADMIN') and hasRole('ROLE_DBA')")
            .anyRequest().authenticated()
            .and()
            .formLogin().loginPage("/signin").usernameParameter("login").passwordParameter("password").permitAll()
            .and()
            .formLogin().defaultSuccessUrl("/playlist")
            .and()
            .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));

    }
}
