package ua.pp.kaeltas.spring.view.streaming;

/**
 * http://datum-bits.blogspot.com/2013/01/implementing-http-byte-range-requests_30.html
 *
 * net/balusc/webapp/FileServlet.java
 *
 * Copyright (C) 2009 BalusC
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library.
 * If not, see .
 */

public class DownloadConstants {
    public static final String INPUT_STREAM = "inputStream";
    public static final String CONTENT_TYPE = "contentType";

    public static final String CONTENT_LENGTH = "contentLength";
    public static final String FILENAME = "filename";
    public static final String LAST_MODIFIED = "lastModified";
}

