package ua.pp.kaeltas.utils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
//import java.util.Base64;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 * Created by kaeltas on 31.01.15.
 */
public class DES {
    public static void main(String[] args) {

        String strDataToEncrypt;
        String strCipherText;
        String strDecryptedText;

        try {
            /**
             *  Step 1. Generate a DES key using KeyGenerator
             */
//            KeyGenerator keyGen = KeyGenerator.getInstance("DES");
//            SecretKey secretKey = keyGen.generateKey();
//            System.out.println("secretKey="+Base64.getEncoder().encode(secretKey.getEncoded()));

            byte[] encodedKey     = new Base64().decode("yK2MihODLws=");
            SecretKey secretKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "DES");


            Cipher desCipher = Cipher.getInstance("DES");

            /**
             *  Step 3. Initialize the Cipher for Encryption
             */

            desCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            /**
             *  Step 4. Encrypt the Data
             *  		1. Declare / Initialize the Data. Here the data is of type String
             *  		2. Convert the Input Text to Bytes
             *  		3. Encrypt the bytes using doFinal method
             */
            /*strDataToEncrypt = "kaeltas@ukr.net";
            byte[] byteDataToEncrypt = strDataToEncrypt.getBytes();
            byte[] byteCipherText = desCipher.doFinal(byteDataToEncrypt);
            strCipherText = Base64.getEncoder().encode(byteCipherText);
            System.out.println("Enc1=" + strCipherText);

            strDataToEncrypt = "33Kimongu25";
            byteDataToEncrypt = strDataToEncrypt.getBytes();
            byteCipherText = desCipher.doFinal(byteDataToEncrypt);
            String strCipherText2 = Base64.getEncoder().encode(byteCipherText);
            System.out.println("Enc2=" + strCipherText2);*/

            /**
             *  Step 5. Decrypt the Data
             *  		1. Initialize the Cipher for Decryption
             *  		2. Decrypt the cipher bytes using doFinal method
             */

            desCipher = Cipher.getInstance("DES");
            desCipher.init(Cipher.DECRYPT_MODE, secretKey);
            //desCipher.init(Cipher.DECRYPT_MODE,secretKey);
            //byte[] byteDecryptedText = desCipher.doFinal(Base64.getDecoder().decode(strCipherText));
            byte[] byteDecryptedText = desCipher.doFinal(new Base64().decode("+gZnG7xp+iURy3FkEen2NA=="));
            strDecryptedText = new String(byteDecryptedText);
            System.out.println("Dec1=" + strDecryptedText);

            //byteDecryptedText = desCipher.doFinal(Base64.getDecoder().decode(strCipherText2));
            byteDecryptedText = desCipher.doFinal(new Base64().decode("Jwp7QMytxQGdd/RBGBRepw=="));
            String strDecryptedText2 = new String(byteDecryptedText);
            System.out.println("Dec2=" + strDecryptedText2);

        } catch (NoSuchAlgorithmException noSuchAlgo) {
            System.out.println(" No Such Algorithm exists " + noSuchAlgo);
        } catch (NoSuchPaddingException noSuchPad) {
            System.out.println(" No Such Padding exists " + noSuchPad);
        } catch (InvalidKeyException invalidKey) {
            System.out.println(" Invalid Key " + invalidKey);
        } catch (BadPaddingException badPadding) {
            System.out.println(" Bad Padding " + badPadding);
        } catch (IllegalBlockSizeException illegalBlockSize) {
            System.out.println(" Illegal Block Size " + illegalBlockSize);
        }
    }

}