package ua.pp.kaeltas.exception;

/**
 * Created by kaeltas on 03.02.15.
 */
public class VkApiException extends Exception {
    public VkApiException() {
    }

    public VkApiException(String message) {
        super(message);
    }

    public VkApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
