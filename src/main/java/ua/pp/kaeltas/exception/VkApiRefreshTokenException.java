package ua.pp.kaeltas.exception;

/**
 * Created by kaeltas on 03.02.15.
 */
public class VkApiRefreshTokenException extends VkApiException {
    public VkApiRefreshTokenException() {
    }

    public VkApiRefreshTokenException(String message) {
        super(message);
    }

    public VkApiRefreshTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
