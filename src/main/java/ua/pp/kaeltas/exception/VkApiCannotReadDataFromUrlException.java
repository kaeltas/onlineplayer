package ua.pp.kaeltas.exception;

/**
 * Created by kaeltas on 03.02.15.
 */
public class VkApiCannotReadDataFromUrlException extends VkApiException {
    public VkApiCannotReadDataFromUrlException() {
    }

    public VkApiCannotReadDataFromUrlException(String message) {
        super(message);
    }

    public VkApiCannotReadDataFromUrlException(String message, Throwable cause) {
        super(message, cause);
    }
}
