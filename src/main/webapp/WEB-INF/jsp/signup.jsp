<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<div class="row">
    <div class="col-sm-offset-4">
        <h3>SignUp</h3>
    </div>
</div>

<c:if test="${param.success eq true}">
    <div class="row">
        <div class="col-sm-offset-4 col-sm-6 alert alert-success">
            Registration successful!
        </div>
    </div>
</c:if>

<div class="row">
    <form:form id="userSignUp" modelAttribute="user" cssClass="form-horizontal">
        <div class="form-group">
            <label for="login" class="col-sm-4 control-label">Login</label>
            <div class="col-sm-6">
                <form:input path="login" cssClass="form-control" placeholder="Login"/>
                <form:errors path="login"/>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-4 control-label">Email</label>
            <div class="col-sm-6">
                <form:input path="email" cssClass="form-control" placeholder="Email"/>
                <form:errors path="email"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-6">
                <form:password path="password" cssClass="form-control" placeholder="Password"/>
                <form:errors path="password"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password_again" class="col-sm-4 control-label">Password-again</label>
            <div class="col-sm-6">
                <input type="password" id="password_again" name="password_again" class="form-control" placeholder="Password-again"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-2">
                <button type="submit" class="btn btn-primary">SignUp</button>
            </div>
        </div>
    </form:form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#login").focus();

        $("#userSignUp").validate({
            rules: {
                login: {
                    required: true,
                    minlength: 3,
                    remote: {
                        url: "<spring:url value="/signup/isloginavailable"/>",
                        type: "GET",
                        data: {
                            login: function () {
                                return $("#login").val();
                            }
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<spring:url value="/signup/isemailavailable"/>",
                        type: "GET",
                        data: {
                            email: function () {
                                return $("#email").val();
                            }
                        }
                    }
                },
                password: {
                    required: true,
                    minlength: 5
                },
                password_again: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            highlight: function (element) {
                $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
            },
            unhighlight: function (element) {
                $(element).closest(".form-group").removeClass("has-error").addClass("has-success");
            },
            messages: {
                login: {
                    remote: "This login has already been taken!"
                },
                email: {
                    remote: "This email has already been taken!"
                },
                password_again: "Please enter the same password again."
            }
        });
    });
</script>