<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 27.01.15
  Time: 10:29
  To change this template use File | Settings | File Templates.

  http://jplayer.org/latest/developer-guide/#jPlayer-play
  http://jplayer.org/latest/demo-02-jPlayerPlaylist/?theme=0
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<h4><c:out value="${playPlaylist.title}"/> <small class="pull-right"><c:out value="${playPlaylist.user.login}"/></small></h4>

<script type="text/javascript">
  $(document).ready(function(){
    var myPlaylist = new jPlayerPlaylist({
      jPlayer: "#jquery_jplayer_1",
      cssSelectorAncestor: "#jp_container_1"
    }, [
      <c:set var="tracksCounter" value="0"/>
      <c:forEach items="${playPlaylist.tracks}" var="track">
        <c:set var="tracksCounter" value="${tracksCounter + 1}"/>
        {
          title:"<c:out value="${track.title}"/>",
          artist:"<c:out value="${track.artist}"/>",
          free: true,
          mp3:"<spring:url value="/getaudio/${track.ownerId}_${track.audioId}"/>"
        }
        <c:if test="${tracksCounter lt playPlaylist.tracks.size()}">
          <c:out value=","/>
        </c:if>
      </c:forEach>

    ], {
      playlistOptions: {
        enableRemoveControls: true,
        shuffleTime : 0
      },
      swfPath: "/js",
      supplied: "oga, mp3",
      smoothPlayBar: true,
      keyEnabled: true,
      volume: 0.2,
      useStateClassSkin: true,
      autoBlur: false,
      remainingDuration: true,
      toggleDuration: true
    });


  });
</script>


<div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
  <div class="jp-type-playlist">
    <div id="jquery_jplayer_1" class="jp-jplayer" style="width: 480px; height: 270px;">
    </div>
    <div class="jp-gui">
      <div class="jp-interface">
        <div class="jp-progress">
          <div class="jp-seek-bar" style="width: 100%;">
            <div class="jp-play-bar" style="width: 0%;"></div>
          </div>
        </div>
        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
        <div class="jp-controls-holder">
          <div class="jp-controls">
            <button class="jp-previous" role="button" tabindex="0">previous</button>
            <button class="jp-play" role="button" tabindex="0">play</button>
            <button class="jp-next" role="button" tabindex="0">next</button>
            <button class="jp-stop" role="button" tabindex="0">stop</button>
          </div>
          <div class="jp-volume-controls">
            <button class="jp-mute" role="button" tabindex="0">mute</button>
            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
            <div class="jp-volume-bar">
              <div class="jp-volume-bar-value" style="width: 80%;"></div>
            </div>
          </div>
          <div class="jp-toggles">
            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
            <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
          </div>
        </div>
        <div class="jp-details" style="display: none;">
          <div class="jp-title" aria-label="title">&nbsp;</div>
        </div>
      </div>
    </div>
    <div class="jp-playlist">
      <ul style="display: block;"><li class="jp-playlist-current"><div><a href="javascript:;" class="jp-playlist-item-remove">×</a><a href="javascript:;" class="jp-playlist-item jp-playlist-current" tabindex="0">Cro Magnon Man <span class="jp-artist">by The Stark Palace</span></a></div></li></ul>
    </div>
    <div class="jp-no-solution" style="display: none;">
      <span>Update Required</span>
      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
    </div>
  </div>
</div>
