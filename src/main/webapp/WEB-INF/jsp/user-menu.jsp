<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 27.01.15
  Time: 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<security:authentication var="principal" property="principal" />
<a href="<spring:url value="/account"/>">MyAccount [<c:out value="${principal.username}"/>]</a><br/>
<a href="<spring:url value="/playlist"/>">Edit playlist</a><br/>
<a href="<spring:url value="/logout"/>">Logout</a>