<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<form:form style="margin-top: 7px;" action="/signin" method="post" cssClass="form-inline">

  <%--<c:if test="${param.logout ne null}">
    <div class="alert alert-info">
      You have been logged out.
    </div>
  </c:if>--%>


    <div class="form-group form-group-sm">
      <label for="inputLogin" class="sr-only">Login</label>
      <input type="text" id="inputLogin" name="login" class="form-control" placeholder="Login" required>
    </div>
    <div class="form-group form-group-sm">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
    </div>

  <%--<div class="checkbox">
      <label>
          <input type="checkbox" value="remember-me"> Remember me
      </label>
  </div>--%>
  <button class="btn btn-primary" type="submit">Sign in</button>
</form:form>



