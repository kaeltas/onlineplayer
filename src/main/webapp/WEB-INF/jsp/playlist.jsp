<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 28.01.15
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<script type="text/javascript">
  $(document).ready(function (){
    //$('#playlistTab a:first').tab('show') // Select first tab
    if ($('#playlistTab a[href="#playlist_${activePlaylist}"]').length == 0) {
      $('#playlistTab a:first').tab('show') // Select first tab
    } else {
      $('#playlistTab a[href="#playlist_${activePlaylist}"]').tab('show') // Select provided tab
    }

    $('.triggerDelete').click(function (e) {
      e.preventDefault();
      $('#confirmDeletePlaylistModal input[name=playlistDeleteId]').val($(this).val());
      $('#confirmDeletePlaylistModal').modal();
    });

    $("#triggerNewPlaylist").click(function (e) {
      e.preventDefault();
      $("#newPlaylistModalLabel").html("New playlist");
      $("#newPlaylistModal form").attr("action", $(location).attr('pathname'));
      $("#newPlaylistModal form input[name='title']").val("");
      $("#newPlaylistModal form input[name='description']").val("");
      $("#newPlaylistModal form input[name='isPrivate']").prop("checked", true);
      $("#newPlaylistModal").modal();
    });

    $("#playlistTabContent .triggerEditPlaylist").click(function (e) {
      e.preventDefault();
      var plid = $(this).val();

      $.getJSON("/playlist/edit/"+plid, "",
              function (data) {
                if (data.title != null && data.isPrivate != null) {
                  $("#newPlaylistModalLabel").html("Edit playlist");
                  $("#newPlaylistModal form").attr("action", "/playlist/edit/"+plid);
                  $("#newPlaylistModal form input[name='title']").val(data.title);
                  $("#newPlaylistModal form input[name='description']").val(data.description);
                  $("#newPlaylistModal form input[name='isPrivate']").prop("checked", data.isPrivate);
                  $("#newPlaylistModal").modal();
                } else {
                  alert("Error! Try again later!");
                }
              }
      ).fail(function(e) {
        alert("Error! Try again later!");
      });


    });

  });
</script>

<div role="tabpanel">
  <ul id="playlistTab" class="nav nav-tabs">
    <c:forEach items="${user.playlists}" var="playlist">
      <li title="<c:out value="${playlist.title}"/><c:if test="${playlist.isPrivate}"> (private)</c:if>">
        <a href="#playlist_${playlist.id}" data-toggle="tab">
          <c:out value="${playlist.title}"/>
          <c:if test="${playlist.isPrivate}">*</c:if>
          <button type="button" class="close closeInnerA triggerDelete" value="${playlist.id}">
            <span aria-hidden="true">&times;</span>
          </button>
        </a>
      </li>
    </c:forEach>
    <!-- Button "Add new playlist" trigger modal -->
    <li>
      <button id="triggerNewPlaylist" title="Add new playlist" type="button" style="margin-top: 6px; margin-left: 4px;" class="btn btn-sm btn-primary">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
      </button>
    </li>
  </ul>
  <div id="playlistTabContent" class="tab-content">
    <c:forEach items="${user.playlists}" var="playlist">
      <div class="tab-pane fade active in" id="playlist_${playlist.id}">
        <div style="margin:5px;">
          <!-- Button "Add tracks" trigger modal -->
          <button type="button" class="btn btn-primary triggerAddTracks" value="${playlist.id}">
            Add tracks
          </button>
          <!-- Button "Edit playlist" trigger modal -->
          <button type="button" class="btn btn-primary triggerEditPlaylist" value="${playlist.id}">
            Edit playlist
          </button>

          <div>
            <strong>Description:</strong> <c:out value="${playlist.description}"/>
          </div>

          <table class="table table-condensed table-striped table-hover fixed-layout">
            <thead>
            <tr>
              <th class="col-sm-3">Artist</th>
              <th>Title</th>
              <th class="col-sm-1">Del</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${playlist.tracks}" var="item">
              <tr>
                <td><c:out value="${item.artist}"/></td>
                <td><a href="<spring:url value="${item.url}"/>"><c:out value="${item.title}"/></a></td>
                <td>
                  <a class="btn btn-xs btn-danger" href="/playlist/${playlist.id}/track/delete/${item.id}">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                  </a>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
    </c:forEach>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    $("#newPlaylistModalForm").validate({
      rules : {
        title : {
          required : true,
          minlength : 1
        }
      },
      highlight: function (element) {
        $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
      },
      unhighlight: function (element) {
        $(element).closest(".form-group").removeClass("has-error").addClass("has-success");
      }
    });
  });

</script>

<!-- Modal: New/Edit playlist -->
<div class="modal fade" id="newPlaylistModal" tabindex="-1" role="dialog" aria-labelledby="newPlaylistModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form:form id="newPlaylistModalForm" modelAttribute="playlist" cssClass="form-horizontal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="newPlaylistModalLabel">New playlist</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-5">
              <form:input path="title" cssClass="form-control" placeholder="Title"/>
              <form:errors path="title"/>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-5">
              <form:input path="description" cssClass="form-control" placeholder="Description"/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
              <div class="checkbox">
                <label>
                  <form:checkbox path="isPrivate" />Is private?
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form:form>
    </div>
  </div>
</div>


<!-- Modal: Confirm Delete Playlist -->
<div class="modal fade" id="confirmDeletePlaylistModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeletePlaylistModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <spring:url var="deletePlaylistUrl" value="/playlist/delete"/>
      <form class="form-horizontal" action="${deletePlaylistUrl}" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="confirmDeletePlaylistModalLabel">Delete playlist</h4>
        </div>
        <div class="modal-body">
          Really delete?
          <input type="hidden" name="playlistDeleteId"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
      </form>
    </div>
  </div>
</div>



<script type="text/javascript">

  function addTrackClick(aid, oid) {
    $("#addTrack input[name=audioId]").val(aid);
    $("#addTrack input[name=ownerId]").val(oid);

    $.post(
            $("#addTrack").attr("action"),
            $("#addTrack").serialize(),
            function() {
              $("#btn"+oid+"_"+aid).html("<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>");
              $("#btn"+oid+"_"+aid).removeClass("btn-info");
              $("#btn"+oid+"_"+aid).addClass("btn-success");
              $("#btn"+oid+"_"+aid).attr("onclick", "").unbind("click");
            }
    );

    return false;
  };

  function playTrackClick(url) {
    $("#jquery_jplayer_1").jPlayer("setMedia", {
      mp3: url
    });
    $("#jquery_jplayer_1").jPlayer("play");

    return false;
  };

  function parseJsonSearchResult(data) {
    var trs = "";
    $.each( data.response.items, function( key, val ) {
      trs += "<tr>";
      trs += "<td><button style=\"margin-right:6px;\" class=\"btn btn-info btn-xs\" onclick=\"return playTrackClick('"+val.url+"');\"><span class=\"glyphicon glyphicon-play\" aria-hidden=\"true\"></span></button>" + val.artist + "</td>";
      trs += "<td>" + val.title + "</td>";
      trs += "<td><button id=\"btn"+val.owner_id+"_"+val.id+"\" class=\"btn btn-info btn-xs\" onclick=\"return addTrackClick("+val.id+", "+val.owner_id+");\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span></button></td>";
      trs += "</tr>";
    });
    return trs;
  }

  var offset = 0;
  var count = 30;
  var loading = false;

  $(document).ready(function () {

    $("#searchResult table").hide();

    $("#playlistTabContent .triggerAddTracks").click(function (e) {
      e.preventDefault();
      $("#addTrack input[name='playlist.id']").val($(this).val());
      $("#addTracksModal").modal();
    });

    //reload page, after addTracksModal closing
    $('#addTracksModal').on('hide.bs.modal', function (e) {
      $(location).attr('href', "/playlist/"+$("#addTrack input[name='playlist.id']").val());
    })

    $("#formSearch .doSearch").click(function (e) {
      e.preventDefault();
      offset = 0;
      $.getJSON("/vksearch",
              {
                q:$("#searchQ").val(),
                count: count,
                offset : offset
              },
              function (data) {
                var trs;
                if (data.error != null) {
                  trs = data.error;
                } else {
                  trs = parseJsonSearchResult(data);
                }

                $("#searchResult table tbody").html(trs);
                $("#searchResult table").show();
                $("#searchResult").scrollTop(0);
              }
      );
    });


    $("#jquery_jplayer_1").jPlayer({
      cssSelectorAncestor: "#jp_container_1",
      swfPath: "/js",
      supplied: "mp3, oga",
      useStateClassSkin: true,
      autoBlur: false,
      smoothPlayBar: true,
      keyEnabled: true,
      remainingDuration: true,
      toggleDuration: true,
      volume: 0.2
    });


    //autoload next search results on scrolling till end
    $("#searchResult").scroll(function () {
      var $this = $(this);
      var height = this.scrollHeight - $this.height(); // Get the height of the div
      var scroll = $this.scrollTop(); // Get the vertical scroll position

      var isScrolledToEnd = (scroll >= height);

      $(".scroll-pos").text(scroll);
      $(".scroll-height").text(height);

      if (isScrolledToEnd && !loading) {
        offset += count;
        loading = true;
        $.getJSON("/vksearch",
                {
                  q: $("#searchQ").val(),
                  count: count,
                  offset: offset
                },
                function (data) {
                  var trs = parseJsonSearchResult(data);

                  $("#searchResult table tbody").append(trs);
                }
        ).always(function() {
                  loading = false;
                }
        );
      }
    });
  });
</script>

<!-- Modal: add track to playlist -->
<div class="modal fade" id="addTracksModal" tabindex="-1" role="dialog" aria-labelledby="addTracksModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addTracksModalLabel">Add tracks</h4>
      </div>
      <div class="modal-body">

        <form style="margin-bottom: 4px;" id="formSearch" class="form-inline">
          <div class="form-group">
            <label class="sr-only" for="searchQ">Search</label>
            <div class="input-group">
              <input type="search" name="search" class="form-control" id="searchQ" placeholder="Search">
            </div>
          </div>
          <button class="btn btn-primary doSearch">Search</button>
        </form>

        <spring:url value="/playlist/track/add" var="addTrackActionUrl"/>
        <form:form modelAttribute="track" action="${addTrackActionUrl}" id="addTrack">
          <form:hidden path="playlist.id"></form:hidden>
          <form:hidden path="ownerId"></form:hidden>
          <form:hidden path="audioId"></form:hidden>
        </form:form>

        <div style="width:97.5%; margin-bottom: 4px;" id="jp_container_1" class="jp-video" role="application" aria-label="media player">
          <div class="jp-type-playlist">
            <div id="jquery_jplayer_1" class="jp-jplayer" style="width: 480px; height: 270px;">
            </div>
            <div class="jp-gui">
              <div class="jp-interface">
                <div class="jp-progress">
                  <div class="jp-seek-bar" style="width: 100%;">
                    <div class="jp-play-bar" style="width: 0%;"></div>
                  </div>
                </div>
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                <div class="jp-controls-holder">
                  <div class="jp-controls">
                    <button class="jp-play" role="button" tabindex="0">play</button>
                    <button class="jp-stop" role="button" tabindex="0">stop</button>
                  </div>
                  <div class="jp-volume-controls">
                    <button class="jp-mute" role="button" tabindex="0">mute</button>
                    <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                    <div class="jp-volume-bar">
                      <div class="jp-volume-bar-value" style="width: 80%;"></div>
                    </div>
                  </div>
                  <div class="jp-toggles">
                    <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                  </div>
                </div>
                <div class="jp-details" style="display: none;">
                  <div class="jp-title" aria-label="title">&nbsp;</div>
                </div>
              </div>
            </div>
            <div class="jp-no-solution" style="display: none;">
              <span>Update Required</span>
              To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
          </div>
        </div>

        <table style="margin-bottom: 0px; width: 97.5%;" class="table table-striped table-hover fixed-layout">
          <thead>
          <tr>
            <th style="width:30%">Artist</th>
            <th style="width:62%">Track</th>
            <th style="width:8%">Add</th>
          </tr>
          </thead>
        </table>
        <div id="searchResult" style="max-height: 400px; overflow-y: auto;">
          <table class="table table-condensed table-striped table-hover fixed-layout">
            <thead>
            <tr>
              <th style="width:30%; padding: 0px;"></th>
              <th style="width:62%; padding: 0px;"></th>
              <th style="width:8%; padding: 0px;"></th>
            </tr>
            </thead>

            <tbody>
            </tbody>
          </table>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>