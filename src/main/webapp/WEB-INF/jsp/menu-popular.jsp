<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 02.02.15
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${popularPlaylists ne null}">
  <h4>Popular in genre</h4>

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <c:forEach items="${popularPlaylists}" var="playlist">
        <div class="panel panel-default">
          <div style="padding: 4px 10px;" class="panel-heading" role="tab" id="playlistHeading_${playlist.id}">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapsePlaylist_${playlist.id}" aria-expanded="false" aria-controls="collapseOne">
                <c:out value="${playlist.title}"/>
              </a>
              <a style="float: right; margin-top: -2px; padding: 0px 10px;" class="btn btn-sm btn-info" href="<spring:url value="/play/${playlist.user.login}/${playlist.id}"/>">Play</a>
            </h4>
          </div>
          <div id="collapsePlaylist_${playlist.id}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="playlistHeading_${playlist.id}">
            <div class="panel-body">
              <%--<div>
                <strong>Tracks count:</strong> <c:out value="${fn:length(playlist.tracks)}"/>
              </div>--%>
              <div>
                <strong>Description:</strong> <c:out value="${playlist.description}"/><br/>
                <strong>Updated:</strong> <fmt:formatDate value="${playlist.lastUpdateDatetime}" pattern="yyyy.MM.dd HH:mm"/>
              </div>

              <%--<table class="table table-condensed table-striped table-hover fixed-layout">
                <thead>
                <tr>
                  <th class="col-sm-5">Artist</th>
                  <th>Title</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${playlist.tracks}" var="item">
                  <tr>
                    <td title="<c:out value="${item.artist}"/>"><c:out value="${item.artist}"/></td>
                    <td title="<c:out value="${item.title}"/>"><a href="<spring:url value="${item.url}"/>"><c:out value="${item.title}"/></a></td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>--%>
            </div>
          </div>
        </div>
    </c:forEach>
  </div>

</c:if>