<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 27.01.15
  Time: 2:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()">
  <c:if test="${user ne null and user.login ne null and user.login ne \"popular\"}">
    <h4>Playlists <small><c:out value="${user.login}"/></small></h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <c:forEach items="${user.playlists}" var="playlist">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="playlistHeading_${playlist.id}">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapsePlaylist_${playlist.id}" aria-expanded="false" aria-controls="collapseOne">
                <c:out value="${playlist.title}"/>
              </a>
              <a style="float: right; margin-top: -6px;" class="btn btn-sm btn-info" href="<spring:url value="/play/${user.login}/${playlist.id}"/>">Play</a>
            </h4>
          </div>
          <div id="collapsePlaylist_${playlist.id}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="playlistHeading_${playlist.id}">
            <div class="panel-body">
              <div>
                <strong>Description:</strong> <c:out value="${playlist.description}"/>
              </div>

              <table class="table table-condensed table-striped table-hover fixed-layout">
                <thead>
                <tr>
                  <th class="col-sm-3">Artist</th>
                  <th>Title</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${playlist.tracks}" var="item">
                  <tr>
                    <td><c:out value="${item.artist}"/></td>
                    <td><a href="<spring:url value="${item.url}"/>"><c:out value="${item.title}"/></a></td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </c:forEach>
    </div>
  </c:if>
</security:authorize>
