<%--
  Created by IntelliJ IDEA.
  User: kaeltas
  Date: 24.01.15
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
  <title><tiles:getAsString name="title"/></title>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
  <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>

  <script type="text/javascript" src="<spring:url value="/js/jplayer-2.9.2/jquery.jplayer.min.js"/>"></script>
  <script type="text/javascript" src="<spring:url value="/js/jplayer-2.9.2/jplayer.playlist.min.js"/>"></script>
  <link type="text/css" rel="stylesheet" href="<spring:url value="/js/jplayer-2.9.2/skin/blue.monday/css/jplayer.blue.monday.min.css"/>">

  <link type="text/css" rel="stylesheet" href="<spring:url value="/css/classic.css"/>">

</head>
<body>
  <tilesx:useAttribute name="current"/>

  <div class="container">
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<spring:url value="/"/>">JBOP</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <%--<li class="${current == 'index' ? 'active' : ''}"><a href="<spring:url value="/"/>">Home</a></li>--%>
            <security:authorize access="hasRole('ROLE_ADMIN')">
              <%--<li class="${current == 'users' ? 'active' : ''}"><a href="<spring:url value="/users"/>">Users</a></li>--%>
              <li><a href="<spring:url value="/getpopular"/>">Update popular playlists</a></li>
            </security:authorize>

          </ul>
          <ul class="nav navbar-nav navbar-right">
            <security:authorize access="! isAuthenticated()">
              <li><tiles:insertAttribute name="signin-form"/></li>
              <li><a style="padding:7px; margin-top:6px;" class="btn" href="<spring:url value="/signup"/>">SignUp</a></li>
            </security:authorize>


            <%--<security:authorize access="! isAuthenticated()">
              <li class="${current == 'signup' ? 'active' : ''}"><a href="<spring:url value="/signup"/>">SignUp</a></li>
              <li class="${current == 'signin' ? 'active' : ''}"><a href="<spring:url value="/signin"/>">SignIn</a></li>
            </security:authorize>--%>
            <security:authorize access="isAuthenticated()">
              <security:authentication var="principal" property="principal" />
              <li class="${current == 'account' ? 'active' : ''}"><a href="<spring:url value="/account"/>">MyAccount [<c:out value="${principal.username}"/>]</a> </li>
              <li class="${current == 'playlist' ? 'active' : ''}"><a href="<spring:url value="/playlist"/>">Edit playlist</a></li>
              <li><a href="<spring:url value="/logout"/>">Logout</a></li>
            </security:authorize>

            <%--<li><a href="#">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>--%>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="col-sm-offset-9 col-sm-3">
      <c:if test="${param.error ne null}">
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          Invalid login/password.
        </div>
      </c:if>
      <c:if test="${param.error eq null and param.logout eq null and authorize ne null}">
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          Please authorize!
        </div>
      </c:if>
    </div>

    <div class="row">

      <div class="col-sm-5 col-sm-offset-2">
        <c:choose>
          <c:when test="${error eq null}">
            <tiles:insertAttribute name="body"/>
          </c:when>
          <c:otherwise>
            <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <strong>Error!</strong> <c:out value="${error}"/>
            </div>
          </c:otherwise>
        </c:choose>

        <hr>
      </div>

      <div class="col-sm-3">
        <div class="row">
          <c:if test="${error eq null}">
            <tiles:insertAttribute name="playlists"/>
          </c:if>
        </div>
        <div class="row">
          <tiles:insertAttribute name="popularPlaylists"/>
        </div>
      </div>

    </div>
  </div>


  <footer class="footer">
    <div style="text-align: center">
      <tiles:insertAttribute name="footer"/>
    </div>
  </footer>
</body>
</html>
